# DKX/Angular/FileUpload

Skeleton for creating custom file uploads in angular forms.

## Installation

```bash
$ npm install --save @dkx/ng-file-upload
```

or with yarn

```bash
$ yarn add @dkx/ng-file-upload
```

## Usage

**Module with form:**

```typescript
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from '@dkx/ng-file-upload';

import {MyFormComponent} from './my-form.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        FileUploadModule,
    ],
    declarations: [
        MyFormComponent,
    ],
})
export class SignModule {}
```

**Form component:**

```typescript
import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {fileMaxSize, fileType} from '@dkx/ng-file-upload';

@Component({
    selector: 'app-form',
    templateUrl: './my-form.component.html',
})
export class MyFormComponent
{

    public form: FormGroup;

    constructor(
        private $fb: FormBuilder,
    ) {
        this.form = this.createForm();
    }

    public onSubmit(values: any): void
    {
        console.log(values.file);
    }

    public fileChanged(files: Array<Blob>): void
    {
        console.log('CHANGED');
        console.log(files);
    }

    private createForm(): FormGroup
    {
        return this.$fb.group({
            file: [[], [fileMaxSize(1024), fileType(['image/jpeg'])]],
        });
    }

}
```

**Form template:**

```html
<form [formGroup]="form" (ngSubmit)="onSubmit(form.value)">

    <dkx-file-upload #upload formControlName="file" [multiple]="true" (changed)="fileChanged($event)">
        <ng-container *ngFor="let file of upload.data">
            {{ file.name }}
            <hr>
        </ng-container>

        <button (click)="$event.preventDefault(); upload.openDialog()">Open file dialog</button>
        <button (click)="$event.preventDefault(); upload.clearData()">Clear all files</button>
    </dkx-file-upload>

    <button type="submit">Save</button>

</form>
```

## Validators

* `fileMaxSize`
* `fileType`
