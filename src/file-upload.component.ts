import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';


@Component({
	selector: 'dkx-file-upload',
	templateUrl: './file-upload.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: FileUploadComponent,
			multi: true,
		},
	],
})
export class FileUploadComponent implements ControlValueAccessor
{


	@Input()
	public multiple: boolean = false;

	@Output()
	public changed: EventEmitter<Array<Blob>> = new EventEmitter;

	@ViewChild('fileUpload', {static: true})
	public fileUpload: ElementRef<HTMLInputElement>;

	public disabled: boolean;

	private _data: Array<Blob> = [];

	private _onChange: (_: any) => void = () => {};

	private _onTouched: () => void = () => {};


	public get data(): Array<Blob>
	{
		return [...this._data];
	}


	public registerOnChange(fn: (_: any) => void): void
	{
		this._onChange = fn;
	}


	public registerOnTouched(fn: () => void): void
	{
		this._onTouched = fn;
	}


	public setDisabledState(isDisabled: boolean): void
	{
		this.disabled = isDisabled;
	}


	public writeValue(obj: any): void
	{
	}


	public nativeOnChange(files: FileList): void
	{
		this.setFiles(Array.from(files));
	}


	public openDialog(): void
	{
		if (this.fileUpload) {
			this.fileUpload.nativeElement.click();
		}
	}


	public clearData(): void
	{
		this.setFiles([]);
	}


	public setFiles(files: Array<Blob>): void
	{
		this.fileUpload.nativeElement.value = '';

		this._data = [...files];
		this._onChange(this._data);
		this._onTouched();

		this.changed.emit(this._data);
	}

}
